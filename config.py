# This is my config for Qtile

import os
# import re
# import socket
import subprocess
from libqtile import hook
from libqtile import qtile
from typing import List  
from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen, ScratchPad, DropDown
from libqtile.lazy import lazy
# from libqtile.utils import guess_terminal
# from libqtile.widget import PulseVolume
# from libqtile.widget.image import Image
from libqtile.dgroups import simple_key_binder

# This is where  I will put some color/theme choices
from colors.catppuccin import colors
# from colors.dracula import colors
# from colors.everforest import colors
# from colors.gruvbox import colors
# from colors.nord import colors
# from colors.onedark import colors
# from colors.solarizeddark import colors
# from colors.tokyonight import colors


#My programmes
mod = "mod4"
myBrowser = 'firefox-bin'
myTerminal = 'kitty'
myTextEditor = 'kitty lvim'
myPrimaryFileManager = 'thunar'
mySecondaryFileManager = 'kitty ranger'
myRecorder = 'obs'
myDrawingApp = 'mypaint'
myVideoEditor = 'kdenlive'
myAudioEditor = 'audacity'
myPhotoEditor = 'gimp'
myVirtualbox = 'virtualbox'
myVideoPlayer = 'vlc'
myPrimaryMenu = 'rofi -show drun -show-icons'
myEmailCliant = 'thunderbird'



keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc = "Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc = "Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc = "Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc = "Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc = "Move window focus to other window"),

     ### Switch focus of monitors
    Key([mod], "period", lazy.next_screen(), desc='Move focus to next monitor'),
    Key([mod], "comma", lazy.prev_screen(), desc='Move focus to prev monitor'),


    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "j", lazy.layout.shuffle_left(),
        desc = "Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc = "Move window to the right"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_down(),
        desc = "Move window down"),
    Key([mod, "shift"], "i", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc = "Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc = "Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc = "Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc = "Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc = "Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc = "Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(myTerminal), desc = "Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc = "Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc = "Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc = "Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc = "Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc = "Spawn a command using a prompt widget"),

    #Browser
    Key([mod], "b", lazy.spawn(myBrowser), desc = "Launch browser"),

    #Geany
    Key([mod], "t", lazy.spawn(myTextEditor), desc = "Launch lvim"),

    #Email client
    Key([mod], "e", lazy.spawn(myEmailCliant), desc = "Launch thunderbird"),

    #Primary File manager
    Key([mod], "f", lazy.spawn(myPrimaryFileManager), desc = "Lauch primary file manager"),

    #rofq
    Key([mod], "End", lazy.spawn(myPrimaryMenu), desc = "Launch Rofi"),

    #Screenshot hotkey
    # Key([mod], "p", lazy.spawn())
    #Sound
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume 0 +5%")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume 0 -5%")),
    Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute 0 toggle")),

    #Brightness
    # Key([], "XF86MonBrightnessUp", lazy.spawn("lux -a 10%")),
    # Key([], "XF86MonBrightnessDown", lazy.spawn("lux -s 10%")),
]

groups = [Group("\uf269", layout='MonadWide', matches=[Match(wm_class=myBrowser)]),
          Group("", layout='MonadWide'),
          Group("", layout='MonadWide'),
          Group("", layout='MonadWide'),
          Group("", layout='MonadWide', matches=[Match(wm_class=myRecorder)]),
          Group("", layout='MonadWide', matches=[Match(wm_class=myDrawingApp)]),
          Group("", layout='MonadWide', matches=[Match(wm_class=[myVideoEditor, myAudioEditor, myPhotoEditor])]),
          Group("", layout='MonadWide', matches=[Match(wm_class=myVideoPlayer)]),
          Group("", layout='MonadWide', matches=[Match(wm_class=myVirtualbox)]),
          Group("", layout='MonadWide')]

dgroups_key_binder = simple_key_binder(mod)


# Append scratchpad with dropdowns to groups
groups.append(ScratchPad('scratchpad', [
    DropDown('myTerminal', myTerminal, width=0.4, height=0.5, x=0.3, y=0.2, opacity=1),
    DropDown('mySecondaryFileManager', mySecondaryFileManager, width=0.4, height=0.5, x=0.3, y=0.2, opacity=1),
]))
# extend keys list with keybinding for scratchpad
keys.extend([
    Key(["control"], "1", lazy.group['scratchpad'].dropdown_toggle('myTerminal')),
    Key(["control"], "2", lazy.group['scratchpad'].dropdown_toggle('mySecondaryFileManager')),
])


layouts = [
    # layout.Bsp(border_focus = colors[4], margin = 2), 
    layout.MonadTall(border_focus = colors[6], margin = 15),
    layout.MonadWide(border_focus = colors[6], margin = 15),
    layout.RatioTile(border_focus = colors[4], margin = 2),
    layout.TreeTab(border_focus = colors[4], margin = 2),
    layout.Tile(border_focus = colors[4], margin = 2),
    #layout.Floating(border_focus='bd93f9', margin = 4),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    #layout.Matrix(),
    #layout.RatioTile(),
    # layout.Tile(),
    #layout.TreeTab(),
    #layout.VerticalTile(),
    #layout.Zoomy(),
]

widget_defaults = dict(
    font = 'JetBrains Mono',
    fontsize = 20,
    padding = 3,
    background = colors[0],
    foreground = colors[2]
)
extension_defaults = widget_defaults.copy()

# Populating widets
def init_widgets_lists():
    widget_list = [
                widget.Sep(
                    linewidth = 0,
                    padding = 2,
                    # background= colors[6]
                    ),
                widget.CurrentLayoutIcon(
                    padding=1,
                    foreground= colors[5],
                    use_mask= False
                    ),
                widget.Spacer(
                    length=20,
                    ),

                widget.TextBox(
                    text='\ue0c4',
                    padding=0,
                    fontsize=30,
                    background= colors[1],
                    foreground= colors[0]
                    ),
                widget.GroupBox(
                    fontsize = 17,
                    disable_drag = True,
					margin_x = 0,
                    margin_y = 3,
                    padding_y = 5,
                    padding_x = 3,
                    background = colors[1],
					active = colors[0],
                    inactive = colors[2],
                    highlight_color = colors[1],
                    highlight_method = 'line',
                    this_current_screen_border = colors[4],
                    this_screen_border= colors[5],
                    hide_unused= True

                    ),
                widget.Prompt(
                    background= colors[1],
                    foreground= colors[0]

                    ),
                widget.TextBox(
                    text='\u25e2',
                    padding=0,
                    fontsize=50,
                    background = colors[1],
                    foreground = colors[1]),
               widget.CurrentLayout(
					scale = 0.7,
                    background = colors[1]),
                widget.TextBox(
                    text='\u25e2',
                    padding=0,
                    fontsize=50,
                    background = colors[1],
                    foreground = colors[0]),
               widget.WindowName(
				    foreground = colors[1]),
                # widget.Chord(
                #     chords_colors = {
                #         'launch': (colors[0, 2]),
                #     },
                #     name_transform=lambda name: name.upper(),
                #     ),
                # widget.Systray(background = colors[4]),
                # widget.TextBox(
                #     text='\u25e2',
                #     padding=0,
                #     fontsize=50,
                #     background = colors[0],
                #     foreground = colors[3]),
                # widget.Net(
                #     interface = "wlan0",
                #     format = '  {down} ↓↑ {up}',
                #     padding = 2,
                #     # background = colors[3],
                #     foreground = colors[3]                    ), 
                # widget.TextBox(
                #     text='\u25e2',
                #     padding=0,
                #     fontsize=50,
                #     background = colors[3],
                #     foreground = colors[12]),
                widget.CPU(
                format = '  {freq_current}GHz {load_percent}%',
                padding = 2,
                # background = colors[2],
                mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerminal + ' -e btop')},
                foreground = colors[6]
                ),
                # widget.TextBox(
                #     text='\u25e2',
                #     padding=0,
                #     fontsize=50,
                #     # background = colors[2],
                #     foreground = colors[6]),
				# widget.Memory(
    #                 # background = colors[2],
    #                 foreground = colors[5],
    #                 mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerminal + ' -e btop')},
    #                 fmt = '\ue28c  {}',
    #                 measure_mem='G',
    #                 padding = 2
				# 	),
                # widget.TextBox(
                #     text='\u25e2',
                #     padding=0,
                #     fontsize=50,
                #     background = colors[5],
                #     foreground = colors[4]),
                # widget.MemoryGraph(
                #         border_color= colors[5],
                #         fill_color= colors[5],
                #         graph_color = colors[5],
                #         type= 'box'
                #         ),
				widget.ThermalSensor(
                       foreground = colors[4],                       
                        # background = colors[4],
                       threshold = 70,
                       fmt = '  {}',
                       tag_sensor= "Core 0",
                       padding = 5
                       ),
                # widget.TextBox(
                #     text='\u25e2',
                #     padding=0,
                #     fontsize=50,
                #     background = colors[4],
                #     foreground = colors[10]),
                widget.PulseVolume(
					fmt = '  {}',
					foreground = colors[10],
                    # background = colors[10],
					padding = 5,
                    scroll = True,
                    
                    # scroll_
					),
                # widget.TextBox(
                #     text='\u25e2',
                #     padding=0,
                #     fontsize=50,
                #     background = colors[10],
                #     foreground = colors[1]),
                # widget.Battery(
                #         charge_char ='',
                #         discharge_char = '',
                #         format = '  {percent:2.0%} {char}',
                #         foreground = colors[6],
                #     background = foregroundColorTwo,
                #         padding = 2
                #     ),
                # widget.TextBox(
                #     text='\u25e2',
                #     padding=0,
                #     fontsize=50,
                #     background = colors[6],
                #     foreground = colors[10]),
                widget.Clock(format=' %a %d %m %Y |  %I:%M %p',
					foreground = colors[1],
                    # background = colors[1],
					padding = 2
					),
				widget.QuickExit(
					fmt = ' ',
					foreground = colors[6],
					padding = 10
					),
                ]
    return widget_list

# def open_menu(qtile):
#     qtile.cmd_spawn('rofi -show drun')

# A way to keep the bar/widgets seperate per monitor
def init_widgets_screen1():
    widget_screen1 = init_widgets_lists()
    return widget_screen1

def init_widgets_screen2():
    widget_screen2 = init_widgets_lists()
    return widget_screen2

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), margin=6, opacity=0.8, size=30)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), margin=3, opacity=0.8, size=25))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widget_list = init_widgets_lists()
    widget_screen1 = init_widgets_screen1()
    widget_screen2 = init_widgets_screen2()

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start = lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start = lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]


#dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(border_focus = colors[8], float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class = 'kdenlive'),  # gitk
    Match(wm_class = 'gimp'),  # gitk
    Match(wm_class = 'mypaint'),  # gitk
    Match(wm_class = 'ssh-askpass'),  # ssh-askpass
    Match(title = 'branchdialog'),  # gitk
    Match(title = 'pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

#Programms to start on log in
@hook.subscribe.startup_once
def autostart ():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

wmname = "winder-manajer"
